# Operations Manual Repository

This is a component of Antora Test site for generation. 

To generate the site go to [this repository](https://gitlab.com/huw-jones-antora-tech-task/docs-site-playbook) and follow the README.md instructions displayed.
